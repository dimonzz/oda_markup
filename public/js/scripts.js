var Slider = function(){
	var slider = jQuery('.slider');
	var slides = slider.find('.slide');
	var slidesCount = slides.length;
	var currentIndex = 0;
	slider.find('.controls .left').on('click', function(){
		$(slides[currentIndex % 3]).removeClass('active');
		$(slides[currentIndex % slidesCount]).removeClass('active');
		currentIndex--;
		$(slides[currentIndex % 3]).addClass('active');
		currentIndex = currentIndex == -1 ? slidesCount - 1 : currentIndex;
		$(slides[currentIndex % slidesCount]).addClass('active');
	});
	slider.find('.controls .right').on('click', function(){
		$(slides[currentIndex % 3]).removeClass('active');
		$(slides[currentIndex % slidesCount]).removeClass('active');
		currentIndex++;
		$(slides[currentIndex % 3]).addClass('active');
		$(slides[currentIndex % slidesCount]).addClass('active');
	});	
}



$(document).ready(function(){
	Slider();

	var myDatepicker = document.querySelectorAll('input.datepicker');
	for(var i = 0; i < myDatepicker.length; i++){
		myDatepicker[i].DatePickerX.init({
			format           : 'dd/mm/yyyy',
			weekDayLabels    : ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Нд'],
        	shortMonthLabels : ['Січ', 'Лют', 'Бер', 'Кві', 'Тра', 'Чер', 'Лип', 'Сер', 'Вер', 'Жов', 'Лис', 'Гру'],
        	singleMonthLabels: ['Січень', 'Лютий', 'Березень', 'Квітень', 'Травень', 'Червень', 'Липень', 'Серпень', 'Вересень', 'Жовтень', 'Листопад', 'Грудень'],
        	todayButton      : true,
        	todayButtonLabel : 'Сьогодні',
        	clearButton      : true,
        	clearButtonLabel : 'Очистити'
  		});
	};

	$('.gallery-images img').click(function(){
		$('.slider .slide').css('background','url(\'' + $(this).attr('src') + '\')');
		$('.slider .slide .description').html($(this).data('caption'));
	});

	$('.menu .arrow').click(function(e){
		var menuItem = $(this).parent();
		console.log("LOL",menuItem.children('ul'));
		$(menuItem.children('ul')[0]).fadeToggle('slow');
	});

	function highlightMenuItem($item){
		if(!$item.is('li')) return;
		$item.addClass('highlight');
		var $parent = $item.parent().parent();
		if($parent.is('li')){
			highlightMenuItem($parent);
		}
	}

	function unhighlightMenuItem($item){
		if(!$item.is('li')) return;
		$item.removeClass('highlight');
		var $parent = $item.parent().parent();
		if($parent.is('li')){
			unhighlightMenuItem($parent);
		}	
	}

	$('.menu li').mouseover(function(e){
		highlightMenuItem($(this));
	});

	$('.menu li').mouseout(function(e){
		unhighlightMenuItem($(this));
	});
});
